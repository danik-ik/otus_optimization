package twitter;

import com.twitter.hbc.httpclient.BasicClient;
import twitter.files.FileUtils;
import twitter.tweeutils.ClientSupplier;

import java.io.File;
import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class TweeterReader {
    private static final String CONSUMER_KEY = "GPArF3hn3QxTf9v6GrwAczcGO";
    private static final String CONSUMER_SECRET = "7dUNMoPKPWtQZ7TcuhoqWTkuTpkSgRqSGk9vPBfXWPMK8NMXxn";
    private static final String TOKEN = "1908368161-IXatUs3pv25qo6C8gy2Rl7LPWzIz9PsCo9cG45l";
    private static final String SECRET = "S20EJhj1db8MIF5xpv5P9UxvCjBTA5A8JTtreeEZNLKei";

    public void run() {
        BlockingQueue<String> queue = new ArrayBlockingQueue<>(50);

        BasicClient[] clients = new BasicClient[2];
        for (int i = 0; i < clients.length; i++) {
            clients[i] = ClientSupplier.prepareAndConnectDefaultClient(CONSUMER_KEY,
                    CONSUMER_SECRET,
                    TOKEN,
                    SECRET,
                    queue);
        }
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newScheduledThreadPool(10);

        for (int i = 0; i < 50; i++) {
            final String fileSuffix = "" + i;
            executor.execute(()->{
                File resultFile = FileUtils.createFile("C:\\temp\\result" + fileSuffix + ".txt");

                Integer rowCount = 0;
                while (anyLive(clients) && rowCount < 100){
                    FileUtils.writeToFileFromQueue(resultFile, queue);
                    rowCount++;
                }
            });
        }
    }

    private boolean anyLive(BasicClient[] clients) {
        return Arrays.stream(clients)
                .anyMatch(it -> !it.isDone());
    }

    public static void main(String[] args) throws Exception {
        TweeterReader reader = new TweeterReader();
        reader.run();
    }
}
